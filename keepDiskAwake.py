import time
import datetime
import os

statFile = "/sys/block/md0/stat"
logFile = "/log.log" # CHANGE
dummyFile = "/dummyFile" # CHANGE
updateInterval = 60 # seconds
keepAwakeTime = 30 * 60 # seconds
keepAwakeIOCount = 12 # empirically proven

# Clear the log file
open(logFile, "w").close()

def getIOCount():
	with open(statFile, "r") as fobj:
		fileContents = fobj.read()
	stats = fileContents.split()
	readCount = int(stats[0])
	writeCount = int(stats[4])
	return readCount + writeCount

# ==== main loop ==== #

maxIODiff = 0
averageIODiff = 0
diffCount = 0

previousIOCount = getIOCount()
timer = 0
while True:
	ioCount = getIOCount()
	if ioCount > previousIOCount:
		timer = keepAwakeTime
	
	ioDiff = ioCount - previousIOCount
	if ioDiff > maxIODiff:
		maxIODiff = ioDiff
	averageIODiff = (averageIODiff * diffCount + ioDiff) / (diffCount + 1)
	diffCount += 1

	previousIOCount = ioCount

	if timer > 0:
		line = str(ioDiff) + " " + str(maxIODiff) + " " + str(averageIODiff) + " " + datetime.datetime.now().strftime("%H:%M:%S") + "\n"
		with open(dummyFile, "w") as dummy:
			dummy.write(line)
			dummy.flush()
			os.fsync(dummy.fileno()),
		with open(logFile, "a") as log:
			log.write(line)
		previousIOCount += keepAwakeIOCount

	timer -= updateInterval
	time.sleep(updateInterval)
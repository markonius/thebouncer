from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters
from telegram import ChatAction
import time
import traceback
import itertools

import logging
import datetime
import signal
import sys

from functools import wraps
import subprocess
import atexit
import re

# CHANGE
logfile = "/LogDirectory/" + datetime.date.today().strftime("%Y-%m-%d") + ".log"

logging.basicConfig(filename=logfile,
					level=logging.WARNING,
					format='[%(levelname)s]\t[%(asctime)s] <%(name)s> %(message)s')

ALLOWED_ID = 0 #CHANGE
bounceMessages = ["You shouldn't be here. Please leave.",
				"Didn't you hear me? You don't belong here.",
				"Don't push me!",
				"I am logging everything you say, and I have your name and phone number.",
				"You are now blocked and you will be dealt with accordingly."]

conversationsFolder = "/ConversationsDirectory" # CHANGE
blackList = {}

def logBlacklist(update, uid):
	uname = update.message.from_user.username
	with open(conversationsFolder + "/" + uname + "." + str(uid), "a") as conversation:
		conversation.write(update.message.text + "\n")
	if (uid in blackList):
		blackList[uid][1] += 1
	else:
		blackList[uid] = [update.message.from_user, 0]

	if (blackList[uid][1] < len(bounceMessages)):
		update.message.reply_text(bounceMessages[blackList[uid][1]])
	elif blackList[uid][1] == len(bounceMessages):
		updater.bot.send_message(chat_id=ALLOWED_ID,
			text="A wild spammer appears: " + blackList[uid][0].first_name + ", @" + uname)

def restricted(func):
	@wraps(func)
	def wrapped(bot, update, *args, **kwargs):
		# extract user_id from arbitrary update
		try:
			user_id = update.message.chat_id
			if user_id != ALLOWED_ID:
				logBlacklist(update, user_id)
				return
		except:
			logBlacklist(update, user_id)
			return

		return func(bot, update, *args, **kwargs)
	return wrapped

@restricted
def whoami(bot, update):
	ajdi = update.message.chat_id
	update.message.reply_text('You are {}'.format(ajdi))
	print("Identified {}".format(ajdi))

@restricted
def who(bot, update):
	print('who:')
	output = subprocess.check_output(["who"])
	print('output:', output)
	update.message.reply_text("Online users:")
	update.message.reply_text(output.decode("utf-8"))

@restricted
def echo(bot, update):
	update.message.reply_text("Unknown command")

@restricted
def bail(bot, update):
	subprocess.call(["ifconfig", "eth0", "down"])
	subprocess.call(["shutdown", "-h", "now"])

@restricted
def getspammers(bot, update):
	lista = "Spammers:\n"
	for k, v in blackList.items():
		lista += str(k) + ", " + v[0].first_name\
		+ ", @" + v[0].username + ": " + str(v[1]) + "\n"
	update.message.reply_text(lista)

updater = Updater('0:0') # CHANGE

updater.dispatcher.add_handler(CommandHandler('whoami', whoami))
updater.dispatcher.add_handler(CommandHandler("who", who))
updater.dispatcher.add_handler(CommandHandler("bail", bail))
updater.dispatcher.add_handler(CommandHandler("getspammers", getspammers))
updater.dispatcher.add_handler(MessageHandler(Filters.text, echo))
updater.dispatcher.add_handler(MessageHandler(Filters.command, echo))

updater.start_polling()

def startWho():
	print('who:')
	output = subprocess.check_output(["who"])
	if not output:
		return
	print('output:', output)
	updater.bot.send_message(chat_id=ALLOWED_ID, text=output.decode("utf-8"))

everythingOK = False
def signal_handler(signal, frame):
	global everythingOK
	updater.bot.send_message(chat_id=ALLOWED_ID, text="Checking out for the day.")
	print("\nSIGINT or SIGTERM, exitting...")
	updater.stop()
	everythingOK = True
	print("Everything is OK!")
	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

def on_exit():
	global everythingOK
	if (not everythingOK):
		print("OK:", everythingOK)
		updater.bot.send_message(chat_id=ALLOWED_ID, text="Shit, something broke.")

atexit.register(on_exit);

sshLog = "ssh.log"
day = ""

updater.bot.send_message(chat_id=ALLOWED_ID, text="Reporting for duty!")
startWho()

def run_tail():
	return subprocess.Popen(['tail', '-F', '--retry', '-n', '1', '/var/log/auth.log'],\
		stdout = subprocess.PIPE, stderr=subprocess.STDOUT)


#tail -F --retry -n 1 /var/log/auth.log
#tailProc = subprocess.Popen(['tail', 'test.txt'],\
#tailProc = subprocess.Popen(['tail', '-F', '--retry', '-n', '1', 'test.txt'],\
tailProc = run_tail()
#while True:
#	line = tailProc.stdout.readline()
#	print(line)

previousMessages = ["one", "two"]
lastMessageTime = datetime.datetime.now()
notReportedCount = 0

linesRead = 0
while True:
	linesRead = linesRead + 1
	if linesRead > 200:
		linesRead = 0
		tailProc.kill()
		tailProc = run_tail()
		updater.bot.send_message(chat_id=ALLOWED_ID, text="Restarted tail")

	line = tailProc.stdout.readline()
	line = line.decode('utf-8')
	#print(line)

	if line.startswith("tail"):
		updater.bot.send_message(chat_id=ALLOWED_ID, text=line)
	elif "sshd" in line:
		#print(line)
		connection = False
		session = False

		if "Connection from" in line:
			connection = True
		if "session opened for user" in line:
			session = True

		if connection or session:		
			with open(sshLog, "a") as log:
				date = line.partition(" ")
				date = date[0] + date[2].partition(" ")[0]
				#print(date)
				if date != day:
					day = date
					log.write("\n")

				log.write(line + "\n")

			strippedLine = re.sub(r"\[[0-9]+\]","",line)
			strippedLine = re.sub(r"[A-Z][a-z]{2}[ ]+[0-9]{1,2}[ ]+([0-9]{1,2}:)+[0-9]+","",strippedLine)
			strippedLine = re.sub(r"port [0-9]+","",strippedLine)
			#print(strippedLine)

			thisTime = datetime.datetime.now()
			timeDiff = thisTime - lastMessageTime
			seconds = timeDiff.total_seconds()
			lastMessageTime = thisTime

			if (notReportedCount >= 50 or seconds > 10):
				previousMessages = ["one", "two"]
				if (notReportedCount > 0):
					updater.bot.send_message(chat_id=ALLOWED_ID, text="Ignored " + str(notReportedCount) + " messages in " + str(int(seconds)) + " seconds.")
				updater.bot.send_message(chat_id=ALLOWED_ID, text=line)
			elif (not (strippedLine in previousMessages)):
				updater.bot.send_message(chat_id=ALLOWED_ID, text=line)
				notReportedCount = 0
			else:
				notReportedCount += 1

			previousMessages = [previousMessages[-1]] + [strippedLine]
